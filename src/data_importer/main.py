import os
import argparse
import logging
import timeit
import datetime

from data_importer.setting import *
from data_importer.helpers.gsheet_helper import get_sheet
from data_importer.helpers.log_helper import LogHelper
from data_importer.helpers.data_helper import DataHelper
from data_importer.processors.processor_factory import ProcessorFactory

from arcgis.gis import GIS

logger = LogHelper()
gis = GIS()

parser = argparse.ArgumentParser(description=APP_DESCRIPTION)
operation_mode = OPERATION
data_helper = None

def initialize():
    
    parser.add_argument('action', 
                        help='the data to import', 
                        action='store', 
                        choices=OPERATIONS)
    parser.add_argument('--fetch',  
                        help='fetch data', 
                        action='store_false', 
                        dest='use_local')
    parser.add_argument('--fetch-only',  
                        help='fetch data only', 
                        action='store_true', 
                        dest='fetch_only')
    parser.add_argument('--geocode-off',  
                        help='turn off geocoding', 
                        action='store_false', 
                        dest='use_geocode')
    parser.add_argument('-v',
                        '--version',
                        action='version',
                        version=f'{VERSION}')

    
def get_raw_file(mode: str):
    raw_file = output = os.path.join(SYNC_DIR, RAW_DATA_NAME[mode])
    logger.log('getting raw data ...')
    if args.use_local:
        logger.log(f'read data from sync folder = {raw_file}...')
        if os.path.isfile(raw_file):
            return  True
    else: 
        logger.log('fetching new raw data ...')
        get_result = get_sheet(mode, logger)
        if get_result and os.path.isfile(raw_file):
            return True

    return False

def process_raw_data(mode: str):

    factory = ProcessorFactory(data_helper, logger)
    processor = factory.get_processor(mode)

    result = False
    if not processor is None:
        result = processor.process()
    else:
        logger.log(f'no processor is available ...', 1)

    return result


if __name__ == "__main__":
    start_program_time = timeit.default_timer()
    logger.log('*****  starting data importer *****')
    
    initialize()
    args = parser.parse_args()
    errr_count = 0
    logger.log(f'[args ={args}]',4)

    mode = args.action
    use_local = args.use_local
    use_geocode = args.use_geocode
    fetch_only = args.fetch_only

    # if use_local:
    #     fetch_only = False

    data_helper = DataHelper(use_geocode, logger)
    # get raw data
    raw_data_ok = get_raw_file(mode)
    
    if not fetch_only:
        if raw_data_ok:
            start_import_time = timeit.default_timer()
            logger.log('raw data file exist, do import ...')
            if process_raw_data(mode):
            #if True:
                logger.log('proccessing file OK ... ')
            else:
                logger.log('proccessing file FAILED ... ')
                errr_count += 1
            logger.log(f'[PROCESSING TIME = {timeit.default_timer()-start_import_time}s]')
        else:
            logger.log('raw data file not exist, please check. data import aborting ...')
            errr_count += 1
    else:
        logger.log(f'fetch only operation DONE ...')

    logger.log(f'data import DONE with {errr_count} error/s.')
    logger.log(f'[TOTAL RUNNING TIME = {timeit.default_timer()-start_program_time}s]')
