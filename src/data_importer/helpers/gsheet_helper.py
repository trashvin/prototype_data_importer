from data_importer.setting import *
from gsheets import Sheets
import os

from data_importer.helpers.log_helper import LogHelper

def get_sheet(mode: str, logger:LogHelper):
    try:
        logger.log('starting function = get_sheet')
        logger.log(f'operation = {mode}')

        logger.log('connecting to gsheet ..')
        sheets = Sheets.from_files(CLIENT_SECRET, STORAGE_FILE)

        url = URLS[mode]
        logger.log('getting gsheet ..')
        result_file = sheets.get(url)

        output = os.path.join(SYNC_DIR, RAW_DATA_NAME[mode])
        logger.log(f'result file = {output}')
        result_file.sheets[0].to_csv(output, encoding='utf-8', dialect='excel')
        logger.log('saved file successfully ..')
        return True
    except Exception as ex:
        logger.log(f'error = {ex}',1)
        return False
