import csv
import os
import re
import pycountry as pc
import arcgis
from dateutil.parser import parse
import datetime
from arcgis.gis import GIS
from arcgis.geocoding import geocode, reverse_geocode
from arcgis.geometry import Point

from data_importer.helpers.log_helper import LogHelper

class DataHelper():

    def __init__(self, use_geocode : bool, logger: LogHelper):
        self._logger = logger
        self._use_geocode = use_geocode

        self._logger.log(f'data_helper initiated use_geocode = {self._use_geocode}')
        

    def country_name_check(self, cList, id = ''):
        valid_trvl_history = []
        try:
            for c in cList:
                is_exists = pc.countries.get(name=c)
                if is_exists is not None:
                    valid_trvl_history.append(c)
                else:
                    if c == "USA":
                        valid_trvl_history.append("United States")
                    elif c == "Korea":
                        valid_trvl_history.append("South Korea")
                    elif c == "Dubai" or c == "UAE":
                        valid_trvl_history.append("United Arab Emirates")
                    elif c == "Taiwan" or c == "Vietnam" or c == "Diamond Princess Cruise Ship":
                        valid_trvl_history.append(c)

                    #print("NOT EXISTs: {}".format((c)))
            if len(valid_trvl_history) == 0:
                return ''
            else:
                return ",".join(valid_trvl_history)
        except Exception as ex:
            self._logger.log(f'error processing country name check = {cList} of {id}. error = {ex}',2)
            return ''

    def get_travel_history(self, value, id = ''):
        history = value.split(",")
        travel = []

        for entry in history:
            entry = entry.lstrip(" ")
            entry = entry.rstrip(" ")
            travel.append(entry)

        return self.country_name_check(travel, id)

    def format_case_no(self, num1, num2, places):
        phs = []
        for i in range(num1, num2 + 1):
            phs.append("PH{}".format(str(i).zfill(places)))

        return phs

    def get_links(self, text, places, id =''):
        links = []
        try:
            if re.search(r"PH\d+-\d+|PH\d+", text):
                epi_link = re.findall(r"PH\d+-\d+|PH\d+", text)

                for s in epi_link:
                    range = s[2:]
                    if re.search(r"PH\d+-\d+", s):
                        num1 = int(range[0:range.index('-')])
                        num2 = int(range[range.index('-') + 1:])
                        phs = self.format_case_no(num1, num2, places)
                        for x in phs:
                            links.append(x)
                    else:
                        links.append("PH{}".format(str(int(range[0:])).zfill(places)))

            if len(links) == 0:
                return ''
            else:
                return ",".join(links)
        except Exception as ex:
            self._logger.log(f'error processing links = {text};{places} of {id}. error = {ex}',2)
            return ''

    def get_date(self, value):
        try:
            pDate = parse(value, fuzzy=False)
            return datetime.datetime.strftime(pDate, '%m-%d-%Y' )
        except Exception as ex:
            if value != '' and value.upper() !='FOR VALIDATION':
                self._logger.log(f'error get data = {value}. error ={ex}', 2)
            return ''

    def clean_numeric(self, str):
        str = re.sub('[^\d.]+', '', str)
        if str == "":
            return "0"
        else:
            return str

    def get_status(self, value):
        if value == "Admitted":
            return 'A'
        elif value == "Expired":
            return "E"
        elif value == "Recovered":
            return "R"
        else:
            return ''

    def get_health_status(self, value):
        if value == "For Validation":
            return 'V'
        elif value == "Died":
            return "D"
        elif value == "Recovered":
            return "R"
        elif value == "Mild":
            return "M"
        elif value == "Asymptomatic":
            return "A"
        elif value == "Severe":
            return "S"
        elif value == "Critical":
            return "C"
        else:
            return ''

    def get_province(self, value):
        search_text = re.search("NCR", value)

        if search_text is not None:
            return "METRO MANILA"
        else:
            return value.upper()


    def get_region(self, value):
        # TODO : make this read from a file for flexibility, files load only at start and laced in a dict?
        if value == 'National Capital Region':
            return 'NCR'
        elif value == 'Autonomous Reg. of Muslim Mindanao':
            return 'ARMM'
        elif value == 'Cordillera Administrative Region':
            return 'CAR'
        elif value == 'Ilocos Region':
            return 'I'
        elif value == 'Cagayan Valley':
            return 'II'
        elif value == 'Central Luzon':
            return 'III'
        elif value == 'Calabarzon':
            return 'IV-A'
        elif value == 'Mimaropa':
            return 'Mimaropa'
        elif value == 'Zamboanga Peninsula':
            return 'IX'
        elif value == 'Bicol Region':
            return 'V'
        elif value == 'Central Visayas':
            return 'VII'
        elif value == 'Western Visayas':
            return 'VI'
        elif value == 'Eastern Visayas':
            return 'VIII'
        elif value == 'Northern Mindanao':
            return 'X'
        elif value == 'Davao Region':
            return 'XI'
        elif value == 'Soccsksargen':
            return 'XII'
        elif value == 'Caraga':
            return 'XIII'
        else:
            return ''

    def get_address(self, x, y, id = ''):
        try:
            region = ''
            province = ''
            citymun = ''
            brgy = ''

            if self._use_geocode:
                location = {
                    'Y': y,
                    'X': x,
                    'spatialReference': {
                        'wkid': 4326
                    }
                }
                pt = Point(location)
                # self._logger.log(f'[point = {pt}]',4)
                
                address = reverse_geocode(location=pt)
                # self._logger.log(f'[address = {address}]',4)

                address = address['address']
                # self._logger.log(f'address {address}',4)
                province = self.get_province(address['Subregion'])
                citymun = '' if address['City'] is None else address['City'].upper()
                region = self.get_region(address['Region'])
                brgy = '' if address['Neighborhood'] is None else address['Neighborhood'].upper()

            return {
                'region': region,
                'province': province,
                'citymun': citymun,
                'brgy': brgy
            }
        except Exception as ex:
            self._logger.log(f'error processing address = {x};{y} of id {id}. error = {ex}',2)
            return {
                'region': '',
                'province': '',
                'citymun': '',
                'brgy': ''
            }