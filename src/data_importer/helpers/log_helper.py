from datetime import datetime
import os
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter

from data_importer.setting import *

class LogHelper:
    
    def __init__(self):
        # log levels based on python logging library
        # 0-NOTSET; 10-DEBUG ; 20 - INFO ; 30 - WARN ; 40 - ERROR ; 50 - CRITICAL
       
        trace_log = "data_importer_trace.log"
        result_log = "result.log"
        log_level = 20

        self._logger = logging.getLogger("data_importer")
        log_formatter = Formatter(fmt='%(asctime)s [%(levelname)s] : %(message)s  [%(threadName)s]',
                                  datefmt='%d-%b-%y %H:%M:%S')

        # console
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)

        # create log file of max size 1MB and keep the last 10
        file_handler = RotatingFileHandler(trace_log, maxBytes=1048576, backupCount = 10)

        file_handler.setFormatter(log_formatter)

        self._logger.addHandler(file_handler)

        if DEBUG_MODE:
            self._logger.addHandler(console_handler)
            log_level = 10

        self._logger.setLevel(int(log_level))
    
    def log(self, message, level=3):
        
        # levels : 1 - ERROR ; 2 - WARNING ; 3 - INFO ; 4 - DEBUG
        if level == 1:
            self._logger.error(f" *** ERROR! {message}", exc_info = True)
        elif level == 2:
            self._logger.warning(f" *** WARNING!{message}")
        elif level == 3:
            self._logger.info(message)
        else: # level = 4 and beyond
            self._logger.debug(f"{message}")

    