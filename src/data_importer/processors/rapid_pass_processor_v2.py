import csv
import os
import re
import pycountry as pc
import arcgis
from dateutil.parser import parse
import datetime
from arcgis.gis import GIS
from arcgis.geocoding import geocode, reverse_geocode
from arcgis.geometry import Point

from data_importer.setting import *
from data_importer.helpers.log_helper import LogHelper
from data_importer.helpers.data_helper import DataHelper

"""
This version will use multi threads
"""
class RapidPassProcessorV2():

    def __init__(self,data_helper: DataHelper, logger: LogHelper):
        self._logger = logger
        self._helper = data_helper
        self._mode = OPERATIONS[1]
        
        self._result_file = os.path.join(
            OUT_DIR[self._mode], 
            RESULT_NAME[self._mode]
        )

        self._logger.log(f'RapidPassProcessor initiated ...')

    def process(self):
        try:
            raw_file = os.path.join(SYNC_DIR, RAW_DATA_NAME[self._mode])
            attrList = []
            self._logger.log(f'file to process = {raw_file}',4)
            columns = ['Loc_name',
                'Status',
                'Score',
                'Match_addr',
                'LongLabel',
                'ShortLabel',
                'Addr_type',
                'Type',
                'PlaceName',
                'Place_addr',
                'Phone',
                'URL',
                'Rank',
                'AddBldg',
                'AddNum',
                'AddNumFrom',
                'AddNumTo',
                'AddRange',
                'Side',
                'StPreDir',
                'StPreType',
                'StName',
                'StType',
                'StDir',
                'BldgType',
                'BldgName',
                'LevelType',
                'LevelName',
                'UnitType',
                'UnitName',
                'SubAddr',
                'StAddr',
                'Block',
                'Sector',
                'Nbrhd',
                'District',
                'City',
                'MetroArea',
                'Subregion',
                'Region',
                'RegionAbbr',
                'Territory',
                'Zone',
                'Postal',
                'PostalExt',
                'Country',
                'LangCode',
                'Distance',
                'X',
                'Y',
                'DisplayX',
                'DisplayY',
                'Xmin',
                'Xmax',
                'Ymin',
                'Ymax',
                'ExInfo']

            records_processed = 0
            number_of_records = 0
            error_count = 0
            #determine the total lines
            with open(raw_file, encoding="latin-1") as file:
                number_of_records = sum(1 for line in file)

            number_of_iterations = int(number_of_records / CHUNK_SIZE) + 1
            self._logger.log(f'total of lines to process = {number_of_records}')
            self._logger.log(f'total of iteration = {number_of_iterations}')

            for iteration in range(number_of_iterations):
                with open(raw_file, encoding="latin-1") as csv_file:
                    read_line = csv.reader(csv_file, delimiter=',')
                    row_count = 0
                    attrList = []
                    for row in read_line:
                        # self._logger.log(f'{(0 + (CHUNK_SIZE * iteration))}',4)
                        if row_count > (0 + (CHUNK_SIZE * iteration)):
                            # Origin fileds - 10, 11, 12
                            # Destination fields - 14, 15, 16
                            sLineAddress = "{}, {}, {}, Philippines".format(row[14], row[15], row[16])
                            # self._logger.log(f'address = {row[14]}, {row[15]}, {row[16]}')
                            
                            address = geocode(sLineAddress)[0]
                            obj = []
                            obj.append(row[0])
                            obj.append(address['address'])
                            obj.append(address['score'])

                            for col in columns:
                                attr = address['attributes']
                                obj.append(attr[col])

                            attrList.append(obj)

                            if row_count % 50 == 0:
                                self._logger.log(f'total processed cases = {row_count}',4)

                            if row_count == CHUNK_SIZE * (iteration + 1):
                                break
                        row_count += 1
                        records_processed += 1

                    new_columns = ['id', 'address', 'score']
                    for col in columns:
                        new_columns.append(col)

                    self._logger.log(f'total processed case = {records_processed}',4)

                    out_file = f'{self._result_file}'
                    if MERGE_OUTPUT:
                        self._logger.log(f'this method is not yet supported. switching to a supported method.',2)
                        # out_file.replace('_[X]','')
                    else:
                        out_file = out_file.replace('[X]',f'{iteration}')
                
                    # this method is for MERGE_OUTPUT = False
                    write_success = True
                    try:
                        with open(out_file, mode="w", newline='') as file:
                            writer = csv.writer(file, delimiter=",", quoting=csv.QUOTE_MINIMAL)
                            self._logger.log(f'writing {attrList}')
                            write_success = self._write_to_file(writer, attrList, new_columns, out_file)
                    except FileExistsError:
                        self._logger.log(f'file already exist {out_file}. the current file will be deleted', 2)
                        os.remove(out_file)

                        with open(out_file, mode="w", newline='') as file:
                            writer = csv.writer(file, delimiter=",", quoting=csv.QUOTE_MINIMAL)
                            write_success = self._write_to_file(writer, attrList, new_columns, out_file)

                    if not write_success:
                        error_count += 1

            self._logger.log(f'processed {records_processed} with {error_count} errors...')
            return True

        except Exception as ex:
            self._logger.log(f'error = {ex}',1)
            return False
        
    def _write_to_file(self, writer, data_list, extra_field, file_name):
        try:
            writer.writerow(extra_field)
   
            for data in data_list:
                try:
                    writer.writerow(data)
                except Exception as ex:
                    writer.writerow(["ID: {} | ERROR: {}".format(data[0], ex)])
                    self._logger.log(f'error writing ID {data[0]}',2)

            self._logger.log(f'successfully written to the file = {file_name}')
            return True
        except Exception as ex:
            self._logger.log(f'ERROR writing file',1)
            return False