from data_importer.processors.rapid_pass_processor_v1 import RapidPassProcessorV1
from data_importer.processors.rapid_pass_processor_v2 import RapidPassProcessorV2
from data_importer.processors.rapid_pass_processor_v3 import RapidPassProcessorV3

from data_importer.processors.trace_covid_processor_v1 import TraceCovidProcessorV1
from data_importer.processors.trace_covid_processor_v2 import TraceCovidProcessorV2
from data_importer.processors.trace_covid_processor_v3 import TraceCovidProcessorV3


from data_importer.setting import *
from data_importer.helpers.log_helper import LogHelper
from data_importer.helpers.data_helper import DataHelper


class ProcessorFactory():

    def __init__(self,data_helper: DataHelper, logger: LogHelper):
        self._logger = logger
        self._helper = data_helper

    
    def get_processor(self, mode:str):
        key = f'{mode}{PROCESSOR_VERSION}'
        self._logger.log(f'creating a {key} processor ...')

        if mode==OPERATIONS[0]:
            processor_dict = {
                'tracecovid1': TraceCovidProcessorV1(self._helper, self._logger),
                'tracecovid2': TraceCovidProcessorV2(self._helper, self._logger),
                'tracecovid3': TraceCovidProcessorV3(self._helper, self._logger),
            }
        else:
            processor_dict = {
                'rapidpass1': RapidPassProcessorV1(self._helper, self._logger),
                'rapidpass2': RapidPassProcessorV2(self._helper, self._logger),
                'rapidpass3': RapidPassProcessorV3(self._helper, self._logger),
            }

        return processor_dict[key]
    