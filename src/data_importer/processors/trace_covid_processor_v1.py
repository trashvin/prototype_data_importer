import csv
import os
import re
import pycountry as pc
import arcgis
from dateutil.parser import parse
import datetime
from arcgis.gis import GIS
from arcgis.geocoding import geocode, reverse_geocode
from arcgis.geometry import Point

from data_importer.setting import *
from data_importer.helpers.log_helper import LogHelper
from data_importer.helpers.data_helper import DataHelper

class TraceCovidProcessorV1():

    def __init__(self,data_helper: DataHelper, logger: LogHelper):
        self._logger = logger
        self._helper = data_helper
        self._mode = OPERATIONS[0]
        
        self._result_file = os.path.join(
            OUT_DIR[self._mode], 
            RESULT_NAME[self._mode]
        )

        self._logger.log(f'TraceCovidProcessor initiated ...')

    def process(self):
        
        was_processed = True
        try:
            raw_file = os.path.join(SYNC_DIR, RAW_DATA_NAME[self._mode])
            attrList = []

            with open(raw_file) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                rows = []
                
                for row in csv_reader:
                    rows.append(row)

                i = 0
                for row in rows:
                    if i != 0:
                        obj = {
                            'case_no': None,
                            'sex': None,
                            'age': None,
                            'nationality': None,
                            'ph_residence_prv': None,
                            'ph_residence_reg': None,
                            'ph_residence_cty': None,
                            'trvl_hstry': None,
                            'epi_link': None,
                            'dt_symptoms': None,
                            'dt_admission': None,
                            'dt_confirmation': None,
                            'oth_disease': None,
                            'status': None,
                            'hlth_status': None,
                            'finl_status': None,
                            'age_group': None,
                            'location': None,
                            'latitude': None,
                            'longitude': None,
                            'res_lat': None,
                            'res_long': None,
                            'dt_annc_pblc': None,
                            'admission': None,
                            'dt_fnl_stat': None
                        }
                        places = len(row[0][2:])

                        obj['case_no']=row[0]
                        obj['sex']=row[1]
                        obj['age']= row[2]
                        obj['nationality']=row[3]

                        obj['trvl_hstry']= self._helper.get_travel_history(row[6], row[0])
                        obj['epi_link']= self._helper.get_links(row[7], places, row[0])
                        obj['orig_epi_link'] = row[7]
                        obj['dt_symptoms']= self._helper.get_date(row[8])
                        obj['dt_admission']= self._helper.get_date(row[9])
                        obj['dt_confirmation']=self._helper.get_date(row[10])
                        obj['oth_disease']=row[14]
                        obj['status']= self._helper.get_status(row[15])
                        obj['hlth_status']= self._helper.get_health_status(row[16])
                        obj['dt_fnl_stat']= row[17]
                        obj['age_group']= row[19]
                        obj['location']= row[20]
                        obj['latitude']= self._helper.clean_numeric(row[21])
                        obj['longitude']= self._helper.clean_numeric(row[22])
                        obj['res_lat']= self._helper.clean_numeric(row[23])
                        obj['res_long']= self._helper.clean_numeric(row[24])
                        obj['dt_annc_pblc']=self._helper.get_date(row[11])
                        obj['admission']=row[12]

                        addr = self._helper.get_address(float(obj['res_long']), float(obj['res_lat']),row[0])
                        obj['ph_residence_prv'] = addr['province']
                        obj['ph_residence_reg']= addr['region']
                        obj['ph_residence_cty']= addr['citymun']
                        obj['ph_residence_brgy'] = addr['brgy']
                        
                        # self._logger.log(f'[Data = {obj}', 4)
                        # self._logger.log(f'processed case = {row[0]}',4)
                        attrList.append(obj)
                    i += 1
                    if i % 50 == 0:
                        self._logger.log(f'total processed cases = {i}',4)

                self._logger.log(f'total processed case = {i}',4)
                try:
                    with open(self._result_file, mode="w", newline='') as file:
                        writer = csv.writer(file, delimiter=",", quoting=csv.QUOTE_MINIMAL)
                        was_processed = self._write_to_file(writer, attrList)
                except FileExistsError:
                    self._logger.log(f'file already exist {self._result_file}. the current file will be deleted', 2)
                    os.remove(self._result_file)

                    with open(self._result_file, mode="w", newline='') as file:
                        writer = csv.writer(file, delimiter=",", quoting=csv.QUOTE_MINIMAL)
                        was_processed = self._write_to_file(writer, attrList)

            return was_processed
        except Exception as ex:
            self._logger.log(f'error processing. error = {ex}',2)
            return False


    def _write_to_file(self, writer, data_list):
        try:
            column_header = ['CASE_NO',
            'SEX',
            'AGE',
            'NATIONALITY',
            'PH_RESIDENCE_PRV',
            'PH_RESIDENCE_REG',
            'PH_RESIDENCE_CTY',
            'TRVL_HSTRY',
            'EPI_LINK',
            'ORIG_EPI_LINK',
            'DT_SYMPTOMS',
            'DT_ADMISSION',
            'DT_CONFIRMATION',
            'OTH_DISEASE',
            'STATUS',
            'HLTH_STATUS',
            'FINL_STATUS',
            'AGE_GROUP',
            'LOCATION',
            'LATITUDE',
            'LONGITUDE',
            'RES_LAT',
            'RES_LONG',
            'DT_ANNC_PBLC',
            'ADMISSION',
            'DT_FNL_STAT']

            writer.writerow(column_header)
            
            for element in data_list:
                column = [element['case_no'],
                        element['sex'],
                        element['age'],
                        element['nationality'],
                        element['ph_residence_prv'],
                        element['ph_residence_reg'],
                        element['ph_residence_cty'],
                        element['trvl_hstry'],
                        element['epi_link'],
                        element['orig_epi_link'],
                        element['dt_symptoms'],
                        element['dt_admission'],
                        element['dt_confirmation'],
                        element['oth_disease'],
                        element['status'],
                        element['hlth_status'],
                        element['finl_status'],
                        element['age_group'],
                        element['location'],
                        element['latitude'],
                        element['longitude'],
                        element['res_lat'],
                        element['res_long'],
                        element['dt_annc_pblc'],
                        element['admission'],
                        element['dt_fnl_stat']]

                # self._logger.log(f'[column = {column}]',4)
                writer.writerow(column)

            self._logger.log(f'successfully written the file {self._result_file}')
            return True
        except Exception as ex:
            self._logger.log(f'ERROR writing file',1)
            return False
