#####################################################################
VERSION = '0.0.1'
APP_DESCRIPTION = 'DCTx Data Importer'
#####################################################################
DEBUG_MODE = True
SHOW_PROGRESS_INTERVAL = 30
PROCESSOR_VERSION = 1
CHUNK_SIZE = 1000
MERGE_OUTPUT = False
#####################################################################
OPERATIONS = ['tracecovid','rapidpass']
SYNC_DIR = 'C:\Temp\SynchedData'
OPERATION = 'tracecovid'
OUT_DIR = {
    'tracecovid':'C:\Temp\TraceCovidOut',
    'rapidpass':'C:\Temp\RapidPassOut'
}

RAW_DATA_NAME = {
    'tracecovid':'trace_raw_data.csv',
    'rapidpass':'rapidpass_raw_data.csv'
}

RESULT_NAME = {
    'tracecovid':'cases_new.csv',
    'rapidpass':'rapid_pass_origin_address_[X].csv'
}

URLS= {
    'tracecovid': 'https://docs.google.com/spreadsheets/d/THISISATESTjeEKF6FPUBq2-pFgmTkHoj5lbVrGLhE',
    'rapidpass' : 'https://docs.google.com/spreadsheets/d/THISISATEST_dFQBZSdvVIVDmIPjA8HSvjM3Rim95oA'
}

CLIENT_SECRET = 'client_secret.json'
STORAGE_FILE = 'storage.json'