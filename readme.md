# Prototype Data Importer

Note : This is a prototype data importer for 2 DCTx projects . The original script was from @dwin

## Project Pre-requisites
- Install python 3.6+
- Install pip
- Install virtualenv
- Clone / Fork / Download this repo

## Virtualenv Setup
- Create 'venv' folder inside the project folder
- Run [note: this is for windows env, feel free to add macos or linux instruction]
```
C:\Your\Project\Dir>virtualenv venv
```  
- Activate the virtual environment 
```
C:\Your\Project\Dir> .\venv\Scripts\activate
```

## Installing the packages
- Inside the virtual env run 
```
C:\Your\Project\Dir>pip install requirements.txt
```

## How to run the program
To run the data importer, go to the 'src' folder and run the python module
```
C:\Your\Project\Dir\src> python -m data_importer.main ACTION [--fetch] [--fetch-only] [--geocode-off]
```
Where:
- ACTION is the data to import [tracecovid  or  rapidpass]
- [--fetch] is a optional argument to force the program to fetch the data from the source. Currently, it only support the GSheet source and the credentials to be used is user based.
- [--fetch-only] is another optional argument telling the importer to fetch the data onle, no additional processing
- [--geocode-off] is an optional argument to turn off geocoding during import. Currently, when geocoding is off, the data derived from geocoding is blank.

### Examples
- importing a locally available raw rapidpass data with geocoding on
```
C:\Your\Project\Dir\src> python -m data_importer.main rapidpass
```
- importing a locally available raw tracecovid data with geocoding on
```
C:\Your\Project\Dir\src> python -m data_importer.main tracecovid
```
- importing a locally available raw rapidpass data with geocoding off
```
C:\Your\Project\Dir\src> python -m data_importer.main rapidpass --geocode-off
```
- importing a to be downloaded raw tracecovid data with geocoding on
```
C:\Your\Project\Dir\src> python -m data_importer.main tracecovid --fetch
```

Note: The source folder, destination folder and other configurable setting is in the setting.py file

## The setting.py
For now, all configurations are kept in this file. Ultimately it has to be place in a json file.

- VERSION : current version of the app
- APP_DESCRIPTION : just a description
- DEBUG_MODE - controls the logging.
- PROCESSOR_VERSION - sets the version of processor to use. See separate discussion on processors
- CHUNK_SIZE - defines how many records are processed per batch. Currently only the rapidpass processor is using this. A value of 1000 means, data will be processed 1000 rows at a time.
- MERGE_OUTPUT - when true, the resulting output will still be a single file regardless of the CHUNK_SIZE. Currently, it only supports non merged output, each chunk will be written on separate files.
- OPERATIONS - supported data. Right now it only support tracecovid and rapidpass data.
- SYNC_DIR - the folder location of the raw data
- OUT_DIR - the folder location where the resulting csv file is written
- RAW_DATA_NAME - the file name of the raw csv file.
- RESULT_NAME - the file name of the resulting csv file.
- URLS -  google sheet location of the raw csv file

## The processors
The list of supported processors are in the data_importer.processors module. Version 1 is the original script from @dwin with minor revisions. Version 2 and 3 are open for experimentation. Feel free to modify. Selecting the processor to use require changing the value of PROCESSOR_VERSION in setting.py, the default is 1.

## Google Drive/Sheet support
The application supports fetching the GSheet from GDrive. For it to run, the user must supply a valid credential in the "client_secret.json.debug" file, then rename it to "client_secret.json"

## Notes
Feel free to clone/fork/download the repo. Its under development, bugs are expected.

## Areas of improvements
- the data cleaning process
- support multi threads on geocoding
- more support on the source of raw files as well as destination locations.
- support running it as a web job in azure or similar platforms
- add script to auto create scheduled task to run the script on a server/computer.


